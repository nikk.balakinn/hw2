# 1. Валидатор паролей. На занятии делали валидацию емейла, тут
#     нужно сделать валидацию пароля по такому же принципу.
#     Придумать критерии надежного пароля, описать их в условиях.


special_chr = {'#', '"', '+', ' '}
passw = input('Enter your password: ')
if len(passw) < 8:
    print('invalid')
if passw.isdigit():
    print('Invalid')
elif passw.isupper():
    print('invalid')
elif passw.islower():
    print('Invalid')
elif  special_chr.intersection(set(passw)):
    print('Invalid')
else:
    print("valid")
#2. Создать список, где все элементы будут кратные 5ти (упражнение на функцию range)

print(list(range(5, 151, 5)))
#3. Нарисовать в консоли ёлочку)
for i in range(7, 0, -1):
    print("~" * (i - 1), end="")
    print("*" * (((7 - i) *2) + 1))
#4. Задать число (input или number=Ваше число) и посчитать количество цифр в нем

i = input("Введите число: ")
if i.isdigit():
    print(len(i))
else:
    print("invalid:(")
#5. Сгенерировать произваольный список и развернуть его
initial_list = [i for i in range(11)]
print(initial_list[::-1])
